# Phoenix Template Engine for Pandoc

A Phoenix template engine that uses [pandoc][0] to convert templates to HTML.
It fully supports EEx tags as well as all input formats supported by pandoc.

## Installation

You must first [install pandoc][1] on the system you intend to run Phoenix on.

The package can be installed by adding `phoenix_pandoc` to the list of dependencies in your `mix.exs`:

```elixir
defp deps do
  [
    {:phoenix_pandoc, "~> 1.0"},
  ]
end
```

## Configuration

To use this template engine with Phoenix, you have to configure it for each
file extension you want to use. Add lines to your `config.exs` similar to:

```elixir
config :phoenix, :template_engines,
  md: PhoenixPandoc.Engine,
  rst: PhoenixPandoc.Engine
```

To enable live reloading for those extensions, you need to edit the patterns
that `phoenix_live_reload` watches. For the above example, your
`config/dev.exs` might look like:

```elixir
config :my_app, MyApp.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{lib/my_app_web/views/.*(ex)$},
      ~r{lib/my_app_web/templates/.*(eex|md|rst)$}
    ]
  ]
```

### Optional Markdown Configuration

Pandoc supports a number of markdown flavors. See the `PhoenixPandoc` module
for details on how to configure the default flavor.

[0]: https://github.com/jgm/pandoc
[1]: https://pandoc.org/installing.html
