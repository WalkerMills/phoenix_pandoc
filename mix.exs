defmodule PhoenixPandoc.MixProject do
  use Mix.Project

  @version "1.1.1"
  @gitlab "https://gitlab.com/WalkerMills/phoenix_pandoc"

  def application do
    [
      applications: [:phoenix],
      extra_applications: [:logger]
    ]
  end

  def project do
    [
      app: :phoenix_pandoc,
      version: @version,
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      source_url: @gitlab,
      docs: docs(),
      description: description()
    ]
  end

  defp deps do
    [
      {:credo, "~> 0.10.0", only: [:dev], runtime: false},
      {:dialyxir, "~> 1.0.0-rc.4", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:phoenix, "~> 1.6"},
      {:phoenix_html, "~> 3.1"}
    ]
  end

  defp package do
    [
      maintainers: ["Walker Mills"],
      licenses: ["MIT"],
      links: %{
        "Source" => @gitlab
      }
    ]
  end

  defp docs do
    [
      extras: ["README.md"],
      main: "readme",
      name: "phoenix_pandoc",
      source_ref: "v#{@version}"
    ]
  end

  defp description do
    """
    Phoenix template engine for Pandoc. Pandoc must already be installed on
    your system.
    """
  end
end
