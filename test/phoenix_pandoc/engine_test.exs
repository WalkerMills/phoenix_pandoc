defmodule PandocEngineTest do
  use ExUnit.Case, async: true

  alias Phoenix.Template.EExEngine
  alias PhoenixPandoc.Engine, as: PandocEngine

  test "test helper test" do
    # This is what the output of Phoenix.HTML.Engine.compile/2 looks like
    nested = [[["" | "a"] | "b"] | "c"]

    assert reduce_phoenix_html(nested) == "abc"
  end

  test "evaluate eex tags" do
    pandoc_output =
      "test/templates/variable.html.eex"
      |> PandocEngine.compile("variable.html")
      |> evaluate_html()

    phoenix_output =
      "test/templates/variable.html.eex"
      |> EExEngine.compile("variable.html")
      |> evaluate_html()

    assert pandoc_output == phoenix_output
  end

  defp evaluate_html(macro) do
    macro
    |> Code.eval_quoted(assigns: %{content: "foo"})
    |> elem(0)
    |> elem(1)
    |> reduce_phoenix_html()
  end

  defp reduce_phoenix_html([]), do: ""
  defp reduce_phoenix_html(list), do: reduce_phoenix_html(list, "")

  defp reduce_phoenix_html([singleton | []], output)
       when is_binary(singleton) do
    singleton <> output
  end

  defp reduce_phoenix_html([head | tail], output)
       when is_binary(head) and is_binary(tail) do
    head <> tail <> output
  end

  defp reduce_phoenix_html([head | tail], output)
       when is_list(head) and is_binary(tail) do
    reduce_phoenix_html(head) <> tail <> output
  end

  defp reduce_phoenix_html([head | tail], output)
       when is_binary(head) and is_list(tail) do
    head <> reduce_phoenix_html(tail, output)
  end
end
