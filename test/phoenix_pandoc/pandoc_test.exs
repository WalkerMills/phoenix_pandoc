defmodule PandocTest do
  use ExUnit.Case, async: true

  alias PhoenixPandoc.Pandoc

  test "calling pandoc succeeds" do
    contents = File.read!("test/templates/static.html.md")

    assert Pandoc.convert(contents, "commonmark", "html") ==
             "<h2>Some static Markdown</h2>\n"
  end

  test "readers and writers exist" do
    assert Pandoc.readers() != []
    assert Pandoc.writers() != []
  end
end
