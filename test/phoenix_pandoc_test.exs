defmodule PhoenixPandocTest do
  use ExUnit.Case, async: true

  test "set markdown flavor" do
    assert PhoenixPandoc.extension_to_reader!(".md") == "commonmark"
  end

  test "get reader from map" do
    assert PhoenixPandoc.extension_to_reader!(".eex") == "html"
  end

  test "reader as extension" do
    # This could be randomized, but that relies on Pandoc.readers/0 working
    # correctly.
    assert PhoenixPandoc.extension_to_reader!(".vimwiki") == "vimwiki"
  end

  test "invalid reader raises" do
    assert_raise(ArgumentError, fn ->
      PhoenixPandoc.extension_to_reader!("foo")
    end)
  end
end
