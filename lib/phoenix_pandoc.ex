defmodule PhoenixPandoc do
  @markdown_flavors [
    "gfm",
    "commonmark",
    "markdown",
    "markdown_phpextra",
    "markdown_strict"
  ]

  @moduledoc ~s"""
  A Phoenix template engine for [pandoc](https://github.com/jgm/pandoc) that
  fully supports EEx templates.

  It tries (not very hard) to map file extensions to pandoc readers, but any
  reader can be used by naming the template like `name.html.reader`. See
  `pandoc --list-input-formats` for a list of supported readers.

  ## Optional Markdown Configuration

  Pandoc supports multiple flavors of Markdown. This module defaults to the
  Github-flavored Markdown reader (`gfm`) for `.md` files. This can be
  overridden in your project's `config.exs` like so:

  ```elixir
    config :phoenix_pandoc, markdown_flavor: "commonmark"
  ```

  `:markdown_flavor` must be set to one of: `#{inspect(@markdown_flavors)}`.
  """

  @markdown_flavor Application.get_env(
                     :phoenix_pandoc,
                     :markdown_flavor,
                     List.first(@markdown_flavors)
                   )
  @extensions %{
    ".eex" => "html",
    ".html" => "html",
    ".md" => @markdown_flavor,
    ".mmd" => "markdown_mmd",
    ".tex" => "latex"
  }

  alias PhoenixPandoc.Pandoc

  @doc ~s"""
  Try to detect the pandoc reader for a file extension.

  Supports the following extension mappings:

  | Extension | Reader |
  | :-: | :-: |
  #{@extensions |> Map.to_list() |> Enum.map(fn {ext, reader} -> "|#{ext}|#{reader}|\n" end)}

  This function raises an `ArgumentError` if the extension is not explicitly
  supported, or a pandoc reader.
  """
  @spec extension_to_reader!(String.t()) :: String.t() | no_return()
  def extension_to_reader!(".md"), do: markdown_flavor()

  def extension_to_reader!(extension) do
    reader = String.trim(extension, ".")

    cond do
      Map.has_key?(@extensions, extension) ->
        Map.get(@extensions, extension)

      reader in Pandoc.readers() ->
        reader

      true ->
        raise(ArgumentError, "Unrecognized file extension #{extension}")
    end
  end

  defp markdown_flavor when @markdown_flavor in @markdown_flavors do
    @markdown_flavor
  end
end
