defmodule PhoenixPandoc.Engine do
  @moduledoc ~s"""
  Implementation of the `Phoenix.Template.Engine` behaviour.

  This module invokes pandoc via the `PhoenixPandoc.Pandoc` wrapper at runtime.
  The templates cannot be converted at compile time since they must be
  evaluated by EEx first (tags may generate markup), and the `assigns` are not
  available then. It relies on `Phoenix.HTML.Engine` to sanitize the HTML.

  """

  @behaviour Phoenix.Template.Engine

  alias PhoenixPandoc.Pandoc

  @doc ~s"""
  Implementation of the `c:Phoenix.Template.Engine.compile/2` callback

  This is invoked by `Phoenix.Template`
  [here](https://github.com/phoenixframework/phoenix/blob/fb2a3f26a49d6f9fdee7e2d03c71ae3fe0d3333c/lib/phoenix/template.ex#L349).
  It is used to compile the template into a function. You should never need to
  call this directly.

  Pandoc will be invoked at runtime if the input template contains any EEx tags.

  ## Parameters

  * `path` - Path of the temlate being compiled
  * `_name` - Name of the template being compiled, stripped of its suffix.

  The second callback parameter is ignored. It could be used to control the
  pandoc writer, but Phoenix requires HTML.
  """
  @impl true
  @spec compile(String.t(), String.t()) :: Macro.t()
  def compile(path, _name) do
    contents =
      EEx.compile_file(path, engine: EEx.SmartEngine, file: path, line: 1)

    reader = path |> Path.extname() |> PhoenixPandoc.extension_to_reader!()

    do_compile(contents, reader)
  end

  @doc ~s"""
  Compile a template into a function.

  If the input template contains no EEx tags, convert it to HTML at compile
  time. Otherwise, the returned macro will invoke Pandoc at runtime to perform
  the coversion after evaluating the tags.
  """
  @spec do_compile(Macro.t(), String.t()) :: Macro.t()
  defp do_compile({:__block__, _, [{:<<>>, _, [body]}]}, reader) do
    body
    |> Pandoc.convert(reader, "html")
    |> EEx.compile_string(engine: Phoenix.HTML.Engine)
  end

  defp do_compile(contents, reader) do
    quote do
      unquote(contents)
      |> Code.eval_quoted([assigns: var!(assigns)], __ENV__)
      |> elem(0)
      |> Pandoc.convert(unquote(reader), "html")
      |> EEx.compile_string(engine: Phoenix.HTML.Engine)
      |> Code.eval_quoted()
      |> elem(0)
    end
  end
end
