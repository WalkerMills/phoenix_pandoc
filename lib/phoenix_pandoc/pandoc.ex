defmodule PhoenixPandoc.Pandoc do
  @moduledoc ~s"""
  A thin wrapper around the pandoc executable.

  Pandoc must be installed on your system for this module to work.
  """

  @doc ~s"""
  Convert a string between markup formats using pandoc.

  ## Parameters

  * `input` - The string to convert with pandoc
  * `from` - The pandoc reader to parse `input` with
  * `to` - The pandoc writer for the output

  When `from` and `to` are the same, the input is returned unmodified.
  """
  @spec convert(String.t(), String.t(), String.t()) :: String.t()
  def convert(input, from, from) do
    input
  end

  def convert(input, from, to) do
    """
    pandoc --from #{from} --to #{to} <<"#{<<3>>}EOF"
    #{input}
    #{<<3>>}EOF
    """
    |> run()
  end

  @doc "List of readers supported by pandoc."
  @spec readers() :: [String.t()]
  def readers do
    "pandoc --list-input-formats"
    |> run()
    |> String.split("\n", trim: true)
  end

  @doc "List of writers supported by pandoc."
  @spec writers() :: [String.t()]
  def writers do
    "pandoc --list-output-formats"
    |> run()
    |> String.split("\n", trim: true)
  end

  @spec run(String.t()) :: String.t()
  defp run(command) do
    port = Port.open({:spawn, command}, [:binary])
    Port.monitor(port)
    receive_all(port, [])
  end

  @spec receive_all(port(), [String.t()]) :: String.t()
  defp receive_all(port, state) do
    receive do
      {^port, {:data, output}} ->
        receive_all(port, [output | state])

      {:DOWN, _ref, :port, ^port, _reason} ->
        state |> Enum.reverse() |> Enum.join()
    end
  end
end
